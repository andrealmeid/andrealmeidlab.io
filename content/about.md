+++
title = "About me"
weight = 30
draft = true
+++

{{< figure class="image main" src="/images/me2.jpg" >}}

Hello! My name is André Almeida, but everybody calls me `tony`.

I'm a software developer and advocate for causes such as privacy, internet
freedom and free software. I also like to study about security and hacking.

I enjoy contributing to free software in my spare time (I've completed Hacktoberfest
challenges 2017 and 2018) and I translate cool projects like Tor on 
Transifex.

--- 

### 199X

Born in São Paulo.

### 2009

Made my first game using GameMaker.  
Wrote my first lines of code in _GameMaker Language_.

### 2013 

Joined IT technical course at ETESP.  
Created a Daft Punk website using old school techniques like `iframe` and
`Adobe Flash Plugins`.

### 2014

Finished High School and Technical Course. My final project was a 
software suite for a travel agency.

### 2015

Joined Unicamp to study Computer Science. Joined CACo (Computer Science and 
Engineering Student Union) as event coordinator.

### 2016

Attended to my first [Cryptorave](https://cryptorave.org/).

Participated on an undergraduate research about information technology applied in education.
Created Intelligent Tutor System.

### 2017

Co-founded Livrecamp, a students group to promote free software.
Presented lectures about free software history, distros GNU/Linux and how to use 
bash and a terminal.
Hosted _installfests_ to help installing GNU/Linux to freshmen students.
 
Undergraduate research about malwares in Tor network focused on botnets.

### 2018

Joined Kryptus as a Software Developer Intern. Worked with low-level 
technologies in order to implement security and cryptography solutions.
 
Co-founded [Enigma](https://enigma.ic.unicamp.br), a study group about privacy,
security and hacking.
