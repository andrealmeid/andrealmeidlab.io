+++
title = "Talks"
weight = 30
draft = false
+++

{{< figure class="image main" src="/images/me3.jpg" >}}

I gave some simple talks and here you can find the `.pdf` files. The language
of the presentation it's the same as the presentation's title (Portuguese/English).

- **Cryptorave**
    - [Criando um grupo de estudos sobre privacidade](talks/cryptorave/enigma.pdf) (_Creating a study group about privacy_)
    - [Introdução ao OpenSSL e libcrypt](talks/cryptorave/openssl.pdf) (_Introduction to OpenSSL and libcrypt_)

- **LKCamp**
    - [Usando o Terminal](talks/lkcamp/terminal.pdf) (_Using the Terminal_)
    - [How programs get run: ELF binaries](talks/lkcamp/elf.pdf)

- **Enigma**
    - [Secomp 2018: Quem mexeu nos meus dados?](talks/enigma/secomp18.pdf) (_Who mess my data?_)
    - [Criptografia simétrica](talks/enigma/simetrico.pdf) (_Symmetric cryptography_)
    - [Criptografia, anonimato e privacidade](talks/enigma/anon.pdf) (_Cryptography, anonymity and privacy_)
    - [Ferramentas anti-vigilância](talks/enigma/vigilancia.pdf) (_Anti-surveillance tools_)
    - [Software livre para designers](talks/enigma/designers.pdf) (_Free software for designers_)
