+++
title = "Resume"
weight = 30
draft = false
+++

{{< figure class="image main" src="/images/me2.jpg" >}}

You can have a look at my compact and succinct **[PDF resume
here](/almeida_resume.pdf)**.  
For a more complete version,
here you can find detailed information regarding the projects
in which I was involved:


## Professional

### Collabora
#### Consultant Software Engineering Intern _2019 - present_

[Collabora](https://www.collabora.com/) is an open source consulting
company, working with Linux Kernel, Multimedia, Graphic Stack, Embedded
Systems and others technologies as well.

As an intern, I'm hacking a virtual multimedia driver.  
_Linux Kernel, Open Source, C, Multimedia_

### Kryptus
#### Software Developer Intern _2018_


[Kryptus](www.kryptus.com/en/) is a cybersecurity company with a wide
portfolio, but mainly focus on trusted hardware and embedded system projects.
Provides a lot of solutions the Brazilian government.
Worked on the following projects:

- I offered support and implemented new features to a disk encryption 
[solution for ATMs](https://www.kryptus.com/solucoes/#setorbancario)  
_C, C++, NASM, BIOS, Virtualization_

- A USB [Cryptography Token](https://www.kryptus.com/criptografia/) PKCS
 #11 compatible. I developed ECC support on the Linux driver  
_C, OpenSSL, Embedded System_

- Developed a secure [voting system](https://bv.fapesp.br/pt/auxilios/94959/vevote-plataforma
-de-hardware-para-votacao-eletronica-verificavel-baseada-em-tecnologia-hsm-hardwa/)
(online and offline), aimed to be easily audited   
 _Django, Python, Continuos Integration, Unit Test, Docker_

---

## Extracurricular activities

### LKCamp
#### Participant _2018 - present_

[LKCamp](http://lkcamp.gitlab.io/) is Campinas' Linux Kernel development study
 group. I attended meetings, presented a lightning talk and submitted a [simple patch](https://git.kernel.org/pub/scm/linux/kernel/git/gregkh/staging.git/log/?qt=author&q=andrealmeid&h=staging-next).  
_Linux Kernel, C, QEMU, Open Source Contribution_

### Enigma
#### Co-founder _2018 - present_

Enigma is a study group created by undergraduate students from Unicamp. Our
focus is mainly in privacy, cyber security, cryptography and hacking. We organize 
talks and activities on a weekly basis. Along with
the technical topics, we also engage on political discussions about the
social impacts of technology.  
_Cyber Security, Privacy, Organization, Management, Talk, Write, Cryptography_

- To promote the creation of our group, we hosted a hacking challenge in
[Capture the Flag](https://ctftime.org/ctf-wtf/) format. We created the web 
platform that hosts the challenge and the challenges itself, about cryptography
reverse engineering and web
exploitation. The challenge were hosted at [decifre.me](https://decifre.me)
- Writer at our [blog](https://enigma.ic.unicamp.br/blog/)
- Presented talks and lectures. My favorite topics are privacy and
cryptography

### LivreCamp
#### Co-founder _2017 - 2018_

LivreCamp was a students group to promote Free Software at Unicamp. I was 
Bash/Terminal instructor, GNU/Linux install supporter.  
_Bash, Terminal, GNU/Linux installation, GNU History_

### Centro Acadêmico da Computação (Students' Union)
#### Events coordinator _2015 - 2016_

Organize transportation to IT events.

---

## Academic

### Torbot
#### Undergraduate Researcher (Unicamp) _2017-2018_

Detecting and mitigating [botnets](https://en.wikipedia.org/wiki/Botnet) 
that use Tor network, through packet and analysis.  
_Reverse Engineering, Tor, Malware and Packet Analysis, Network_

Developed tools:

- [ssh-bot](https://github.com/andrealmeid/ssh-bot):
a simple and harmless botnet command and control that communicate 
via SSH.  
_Python, SSH_
- [injector_gui](https://github.com/LascaTorbot/injector_gui):
network packet analyser, aimed to fire attacks on transport
layer with GUI.  
_Python, PyQT, Scapy_
- [cybercrime-scrapper](https://github.com/andrealmeid/cybercrime-scrapper):
A scrapper to gather information about botnets found on 
[cybercrime-tracker.net](https://cybercrime-tracker.net)


Presented at VII Secomp Unicamp, XII WTD Unicamp and VII EnSI UFBA


### Multimedia Intelligent Tutoring System
#### Undergraduate Researcher (Unicamp) _2017-2018_

An educational system with support to videos, images, songs and text. The
system adapts itself according to student's answer.

Presented at XXV Unicamp
Undergraduate Researcher Congress (_XXV Congresso de Iniciação Científica 
da Unicamp_). [Academic poster](/poster_its.pdf) (in Portuguese).  
_Educational Systems, HTML, CSS, Vanilla JavaScript_

---

## Education

### Unicamp
#### Computer Science Bachelor's Degree

March 2015 - December 2019 (Expected)

### Etesp
#### High School and IT Technical Degree _2012 - 2014_

Developed a software suite for a travel agency

---

## Personal projects
### [Syntax Highlighting package for Atom](https://github.com/andrealmeid/atom-language-leg)
 _2017_
I created a [package for Atom](https://atom.io/packages/language-leg) that adds 
a Syntax Highlighting for LEG, an assembly language created by a Unicamp's 
teacher.

### [Web scraper](https://github.com/andrealmeid/BiSEx) _2016_

I created a web scraper to check for a [business website](https://biva.com.br/)
new offers and notify when there is a fresh one

### [Telegram Bot](https://github.com/leandrohrb/Anti-Baby-Bot) _2016_

I helped create a Telegram Bot to remind people that takes regular pills to 
take them.

---

## Misc
### Hacktoberfest
I completed [Hacktoberfest](https://hacktoberfest.digitalocean.com/) 
challenges 2017 and 2018.

### Translating
I helped to translate some pages and strings of Tor Project and Bitmask 
Android App on Transifex.

### Olympiad in Informatics _2015_
29th position in Brazilian Olympiad in Informatics

### HackAmericas 2018 São Paulo _2018_
1st  place on a mobility hackathon. Our group proposed a fintech business model
